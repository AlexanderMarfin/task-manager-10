package ru.tsc.marfin.tm.api.controller;

public interface IProjectController {

    void showProjectList();

    void createProject();

    void clearProjects();

}
