package ru.tsc.marfin.tm.api.controller;

public interface ICommandController {

    void showSystemInfo();

    void showWelcome();

    void showErrorCommand(String arg);

    void showErrorArgument(String arg);

    void showAbout();

    void showVersion();

    void showCommands();

    void showArguments();

    void showHelp();
}
