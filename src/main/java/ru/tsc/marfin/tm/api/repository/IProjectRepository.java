package ru.tsc.marfin.tm.api.repository;

import ru.tsc.marfin.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    void remove(Project project);

    Project create(String name);

    Project create(String name, String description);

    Project add(Project task);

    void clear();

}

