package ru.tsc.marfin.tm.api.repository;

import ru.tsc.marfin.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
