package ru.tsc.marfin.tm.component;

import ru.tsc.marfin.tm.api.controller.ICommandController;
import ru.tsc.marfin.tm.api.controller.ITaskController;
import ru.tsc.marfin.tm.api.repository.ICommandRepository;
import ru.tsc.marfin.tm.api.repository.ITaskRepository;
import ru.tsc.marfin.tm.api.service.ICommandService;
import ru.tsc.marfin.tm.api.service.ITaskService;
import ru.tsc.marfin.tm.constant.ArgumentConst;
import ru.tsc.marfin.tm.constant.TerminalConst;
import ru.tsc.marfin.tm.controller.CommandController;
import ru.tsc.marfin.tm.controller.TaskController;
import ru.tsc.marfin.tm.repository.CommandRepository;
import ru.tsc.marfin.tm.repository.TaskRepository;
import ru.tsc.marfin.tm.service.CommandService;
import ru.tsc.marfin.tm.service.TaskService;
import ru.tsc.marfin.tm.util.TerminalUtil;

import java.util.Scanner;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    public static void close() {
        System.exit(0);
    }

    public void run(final String[] args){
        if (processArgument(args)) System.exit(0);

        commandController.showWelcome();
        while (true) {
            System.out.println("ENTER COMMAND");
            final String command = TerminalUtil.nextLine();
            processCommand(command);
        }
    }

    public void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.ABOUT:
                commandController.showAbout();
                break;
            case TerminalConst.VERSION:
                commandController.showVersion();
                break;
            case TerminalConst.HELP:
                commandController.showHelp();
                break;
            case TerminalConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConst.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConst.TASK_LIST:
                taskController.showTaskList();
                break;
            case TerminalConst.EXIT:
                close();
                break;
            default:
                commandController.showErrorCommand(command);
                break;
        }
    }

    public void processArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConst.COMMANDS:
                commandController.showCommands();
                break;
            case ArgumentConst.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            default:
                commandController.showErrorArgument(arg);
                break;
        }
    }

    public static boolean processArgument(final String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        processArgument(args);
        return true;
    }

}
