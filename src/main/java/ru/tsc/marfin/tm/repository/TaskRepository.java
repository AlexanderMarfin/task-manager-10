package ru.tsc.marfin.tm.repository;

import ru.tsc.marfin.tm.api.repository.ITaskRepository;
import ru.tsc.marfin.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task create(final String name){
        final Task task = new Task();
        task.setName(name);
        return add(task);
    }

    @Override
    public List<Task> findAll(){
        return tasks;
    }

    @Override
    public void remove(Task task){
        tasks.remove(task);
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public Task create(final String name, final String description){
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        return add(task);
    }

    public Task add(final Task task){
        tasks.add(task);
        return task;
    }

}
