package ru.tsc.marfin.tm.repository;

import ru.tsc.marfin.tm.api.repository.ICommandRepository;
import ru.tsc.marfin.tm.constant.ArgumentConst;
import ru.tsc.marfin.tm.constant.TerminalConst;
import ru.tsc.marfin.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    public static final Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show system info."
    );

    public static final Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    public static final Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version."
    );

    public static final Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show terminal commands."
    );

    public static final Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application."
    );

    public static final Command COMMANDS = new Command(
            TerminalConst.COMMANDS, ArgumentConst.COMMANDS,
            "Close commands list."
    );

    public static final Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Show task list."
    );

    public static final Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks."
    );

    public static final Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task."
    );

    public static final Command ARGUMENTS = new Command(
            TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS,
            "Show argument list."
    );

    private final Command[] terminalCommands = new Command[]{
            INFO, ABOUT, VERSION, HELP,
            COMMANDS, ARGUMENTS,
            TASK_CLEAR, TASK_CREATE, TASK_LIST,
            EXIT
    };

    @Override
    public Command[] getTerminalCommands() {
        return terminalCommands;
    }
}
