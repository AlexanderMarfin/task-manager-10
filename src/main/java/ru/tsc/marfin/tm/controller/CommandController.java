package ru.tsc.marfin.tm.controller;

import ru.tsc.marfin.tm.api.controller.ICommandController;
import ru.tsc.marfin.tm.api.service.ICommandService;
import ru.tsc.marfin.tm.model.Command;
import ru.tsc.marfin.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showSystemInfo() {
        final Runtime runtime = Runtime.getRuntime();
        final int availableProcessors = runtime.availableProcessors();
        System.out.println("Available processors (cores): " + availableProcessors);
        final long freeMemory = runtime.freeMemory();
        final String freeMemoryFormat = NumberUtil.formatBytes(freeMemory);
        System.out.println("Free memory: " + freeMemoryFormat);
        final long maxMemory = runtime.maxMemory();
        final String maxMemoryValue = NumberUtil.formatBytes(maxMemory);
        final boolean isMemoryLimit = maxMemory == Long.MAX_VALUE;
        final String maxMemoryFormat = isMemoryLimit ? "no limit" : maxMemoryValue;
        System.out.println("Maximum memory: " + maxMemoryFormat);
        final long totalMemory = runtime.totalMemory();
        final String totalMemoryFormat = NumberUtil.formatBytes(totalMemory);
        System.out.println("Total memory available to JVM: " + totalMemoryFormat);
        final long userMemory = totalMemory - freeMemory;
        final String usedMemoryFormat = NumberUtil.formatBytes(userMemory);
        System.out.println("User memory in JVM: " + usedMemoryFormat);
    }

    @Override
    public void showWelcome() {
        System.out.println("** Welcome to Task-Manager **");
    }

    @Override
    public void showErrorCommand(String arg) {
        System.err.printf("Error! This argument `%s` is not supported \n", arg);
    }

    @Override
    public void showErrorArgument(String arg) {
        System.err.printf("Error! This argument `%s` is not supported \n", arg);
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Alexander Marfin");
        System.out.println("E-mail: amarfin@t1-consulting.ru");
    }

    @Override
    public void showVersion() {
        System.out.println("1.2.0");
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String name = command.getName();
            if (name == null || name.isEmpty()) continue;
            System.out.println(name);
        }
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command : commands) System.out.println(command);
    }

}
